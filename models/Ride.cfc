/**
 * A bike ride workout thing
*/
component extends="quick.models.BaseEntity" {

	/**
	 * Table definition in Quick
	 * @see https://quick.ortusbooks.com/getting-started/defining-an-entity
	*/
	property name="id";
	/**
	 * Distance ridden in kilometers - easier to manipulate.
	*/
	property name="distance";
	/**
	 * Time length in minutes
	*/
	property name="length";

	/**
	 * An array of geojson points or something like that.
	*/
	property name="route";

	/**
	 * Brief text description of the ride... yup.
	*/
	property name="description";

	/**
	 * Duh, date the ride was recorded
	*/
	property name="createdDate";

	function keyType() {
		return variables._wirebox.getInstance( "UUIDKeyType@quick" );
	}

	// Possible use a subselect to create a generated property?
	// e.g. distance in miles, length in hh:mm format, etc.
	// @see https://quick.ortusbooks.com/getting-started/query-scopes#subselects
}