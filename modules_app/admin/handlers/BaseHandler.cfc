/**
 * Generic handler for admin screens
*/
component secured {
	function onAuthenticationFailure( event, rc, prc ){
		prc.title = "Login";
		event.setView( "admin/login" );
	}
}