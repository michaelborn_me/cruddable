/**
 * Login handler
 * DUH.
*/
component extends="BaseHandler" {

	property name="authenticationService" inject="authenticationService@Admin";

	// Show the login form OR process the submitted login
	function login( event, rc, prc ){
		// basically, check to make sure they submitted any creds at all!
		if ( !rc.keyExists( "username" )
			|| !rc.keyExists( "password" )
			|| rc.username == ""
			|| rc.password == ""
		) {
			// TODO: append error message to flash bag.
		} else {
			variables.authenticationService.login( rc.username, rc.password );
			// TODO: Use a flash bag/thing to store the last url and redirect back to it.
			relocate( "admin.dashboard" );
		}
	}

	// log out the user
	function logout( event, rc, prc ){
		variables.authenticationService.logout( );
	}

}