/**
 * Manage bike rides in admin... stufff...
*/
component extends="BaseHandler" secured {
	property name="RideService" inject="entityService:Ride";

	public void function index( event, rc, prc ){
		prc.title = "Manage Rides";

		prc.rows = RideService
			.orderBy( "createdDate", "desc" )
			.limit( 25 )
			.get();

		// prc.rows = [
		// 	{
		// 		length: 35,
		// 		distance: 8,
		// 		createdDate: "2018-05-11"
		// 	},
		// 	{
		// 		length: 98,
		// 		distance: 35,
		// 		createdDate: "2019-07-26"
		// 	}
		// ];

		prc.columns = [
			{
				label: "Ride Date",
				field: "createdDate",
				width: "20%"
			},
			{
				label: "Ride Length",
				field: "length",
				width="10%"
			},
			{
				label: "Ride Distance",
				field: "distance",
				width: "10%"
			}
		];
	}
}