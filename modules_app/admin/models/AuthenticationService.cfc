/**
 * Work with cbguard to handle authentication.
 */
component {

	/**
	* Must return an object that conforms to `HasPermissionsInterface`.
	* (This may be an implicit implements.)
	*/
	public HasPermissionInterface function getUser(){
		// basically, user component should implement the hasPermissionInterface.
		return application.wirebox.getInstance( "UserService" );
	}

	/**
	* Returns true if the user is logged in.
	* Stored in session. (Kinda not great, but whatever.)
	*/
	public boolean function isLoggedIn(){
		return session.keyExists("loggedin") && session.loggedin;
	}

	public component function login( string username, string password ){
		session.loggedin = true;
		return this;
	}

	public component function logout( ){
		session.loggedin = false;
		return this;
	}

}