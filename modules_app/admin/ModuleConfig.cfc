component {
	this.title = "Admin";
	this.entryPoint = "/admin";
	this.cfmapping = "admin";

	public function configure(){
		// settings = {};

		routes = [
			{pattern="/login", handler="login", action="login"},
			{pattern="/logout", handler="login", action="logout"},
		];
	}
}