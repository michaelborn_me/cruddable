// Inherits from Router and FrameworkSuperType
component{

	function configure(){
			// Turn on Full URL Rewrites, no index.cfm in the URL
			setFullRewrites( true );
			
			// Routing by Convention
			route( "/login" ).withHandler( "login" ).toAction( "login" );
			route( "/logout" ).withHandler( "login" ).toAction( "logout" );
			route( "/:handler/:action?" ).end();
	}
	
}