<cfoutput>
	<div class="container">
		<form action="#event.buildLink( 'admin.login' )#">
			<div class="form-group">
				<label for="username">Username</label>
				<input type="text" name="username" value="" class="form-control">
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" name="password" value="" class="form-control">
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="Login">
			</div>
		</form>
	</div>
</cfoutput>