<cfoutput>
	<table class="table table-striped">
		<thead>
			<tr>
				<cfloop array="#prc.columns#" index="column">
					<th style="width:#column.width#">
						#column.label#
					</th>
				</cfloop>
			</tr>
		</thead>
		<tbody>
			<cfloop array="#prc.rows#" index="row">
				<tr>
					<cfloop array="#prc.columns#" index="column">
						<td>
							#row[ column.field ]#
						</td>
					</cfloop>
				</tr>
			</cfloop>
		</tbody>
	</table>
</cfoutput>